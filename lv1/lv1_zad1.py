'''
Zadatak 1.4.1 Napišite program koji od korisnika zahtijeva unos radnih sati te koliko je placen
po radnom satu. Koristite ugradenu Python metodu - input(). Nakon toga izracunajte koliko
je korisnik zaradio i ispišite na ekran. Na kraju prepravite rješenje na nacin da ukupni iznos
izracunavate u zasebnoj funkciji naziva - total_euro.
'''


work_hours=int(input('Unesite broj radnih sati: '))
payPerHour=float(input('Unesite koliko ste plaćeni po satu: '))

#print('Korisnik je zaradio:',work_hours*payPerHour,'eura.')

def total_euro():
    return work_hours*payPerHour


print('Radni sati:', work_hours ,'h')
print('eura/h:',payPerHour)
print('Ukupno:', total_euro(),'eura')


'''
Zadatak 1.4.2 Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja
nekakvu ocjenu i nalazi se izmedu 0.0 i 1.0. Ispišite kojoj kategoriji pripada ocjena na temelju ¯
sljedecih uvjeta: ´
>= 0.9 A
>= 0.8 B
>= 0.7 C
>= 0.6 D
< 0.6 F
Ako korisnik nije utipkao broj, ispišite na ekran poruku o grešci (koristite try i except naredbe).
Takoder, ako je broj izvan intervala [0.0 i 1.0] potrebno je ispisati odgovaraju ¯ cu poruku. 
'''
while True:
    try:
        percentage=float(input('Unesite broj između 0 i 1: '))
        if percentage<0 or percentage>1:
            print('Unjeli ste broj koji nije u intervalu.')
            continue
        elif percentage>=0.9:
            print('Ocjena je A.')
            break
        elif percentage>=0.8:
            print('Ocjena je B.')
            break
        elif percentage>=0.7:
            print('Ocjena je C.')
            break
        elif percentage>=0.6:
            print('Ocjena je D.')
            break
        elif percentage<0.6:
            print('Ocjena je F.')
            break

    except:
        print('Niste unjeli broj.')


'''
Zadatak 1.4.3 Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji ˇ
sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
(npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovaraju ˇ cu poruku.
'''

numbers=[]

while True:
    number=input('Unesite broj: ')
    if number.isnumeric():
        numbers.append(float(number))
    elif number=='Done': 
        break
    else:
        print('Niste unjeli broj.')

print('Korisnik je unjeo',len(numbers),'brojeva.')
print('Srednja vrijednost unesenih brojeva je',sum(numbers)/len(numbers))
print('Minimalna vrijednost unesenih brojeva je',min(numbers))
print('Maksimalna vrijednost unesenih brojeva je',max(numbers))
        
numbers.sort()
print('Sortirana lista: ')
print(numbers)


'''
Zadatak 1.4.4 Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ song.txt.
Potrebno je napraviti rjecnik koji kao klju ˇ ceve koristi sve razli ˇ cite rije ˇ ci koje se pojavljuju u ˇ
datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (klju ˇ c) pojavljuje u datoteci. ˇ
Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih
'''

fhand=open('LV1/data/song.txt')

word_occurences={}
for line in fhand:
    line=line.rstrip()
    words=line.split()
    for word in words:
        word=word.rstrip(',')
        if word in word_occurences:
            word_occurences[word]=word_occurences[word]+1
        else:
             word_occurences[word]=1

oneOccurenceWords=0
print('Riječi koje se ponavljaju samo jednom su: ')
for word in word_occurences:
    if(word_occurences[word]==1):
        oneOccurenceWords=oneOccurenceWords+1
        print(word)

print('Ima ih: ',oneOccurenceWords)

fhand.close()


'''
Zadatak 1.4.5 Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ SMSSpamCollection.txt
[1]. Ova datoteka sadrži 5574 SMS poruka pri cemu su neke ozna ˇ cene kao ˇ spam, a neke kao ham.
Primjer dijela datoteke:
ham Yup next stop.
ham Ok lar... Joking wif u oni...
spam Did you hear about the new "Divorce Barbie"? It comes with all of Ken’s stuff
a) Izracunajte koliki je prosje ˇ can broj rije ˇ ci u SMS porukama koje su tipa ham, a koliko je ˇ
prosjecan broj rije ˇ ci u porukama koje su tipa spam. ˇ
b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?
'''
fhand=open('LV1/data/SMSSpamCollection.txt',encoding="utf8")

spamWords=0
spamNum=0
hamNum=0
hamWords=0
spamExclamation=0

for line in fhand:
    line=line.rstrip()
    words=line.split()
    if(words[0]=='spam'):
        spamNum=spamNum+1
        spamWords=spamWords+len(words)-1
        if line.endswith('!'):
            spamExclamation=spamExclamation+1
    elif(words[0]=='ham'):
        hamNum=hamNum+1
        hamWords=hamWords+len(words)-1

fhand.close()

print('Prosječna duljina spam poruke je: ',round(spamWords/spamNum))
print('Prosječna duljina ham poruke je: ',round(hamWords/hamNum))
print('Broj spam poruka koje završavaju s uskličnikom je',spamExclamation)
